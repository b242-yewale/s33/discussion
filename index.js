// JavaScript Synchronous vs Asyonchronous
// Synchronous - meaning only one statement is executed at a time

// Ascynchronous - means that we can proceed to execute other statements while thhe time consuming 
// code is running in the background

console.log("Hello");
// conosle.log("Hello again");

// Getting all posts
/*
	Syntax:
		fetch('URL')
*/
console.log(fetch('https://jsonplaceholder.typicode.com/posts'))

// Checking the status of the request
/*
	fetch('URL')
	.then((response) => {})
*/

fetch('https://jsonplaceholder.typicode.com/posts')
.then(response => console.log(response.status));

// Retrieve the content/data from the "Response" object

fetch('https://jsonplaceholder.typicode.com/posts')
.then((response) => response.json())
.then((json) => console.log(json));

// Creates a function using "async" and "await" keywords.

async function fetchData() {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts');

	// Result returns a promise
	console.log(result);
	// Returns the type of response
	console.log(typeof result);
	console.log(result.body);

	// Converts the data from the "Response" object as JSON
	let json = await result.json();
	console.log(json);
}

fetchData();

// Retrieve a specific post

fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response) => response.json())
.then((json) => console.log(json));

// Create a post
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'New Post',
		body: 'Hello World!',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Update a post using PUT method
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PUT',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		id: 1,
		title: 'Updated post',
		body: 'Hello Again!',
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Update a method using the PATCH method
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PATCH',
	headers: {
		'Content-type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Corrected Post'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));

// Delete Post
fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'DELETE'
})

// Filtering Posts
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((json) => console.log(json));

// Retrieve comments of a specific post
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((json) => console.log(json));